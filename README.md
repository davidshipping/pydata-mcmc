# PyData code breakfast 

This repository contains notebook and presentation of the code breakfast of May 22nd 2020 hosted by PyData Amsterdam. Also solutions to the exercises are given in the separate notebook. If you have any questions please contact [David](mailto:david@xomnia.com).

## Some more resources

- [Bayesian Methods for hackers](https://github.com/CamDavidsonPilon/Probabilistic-Programming-and-Bayesian-Methods-for-Hackers)
- [PyMC3 docs](https://docs.pymc.io/) 
- [Explaining the switch point example](https://koaning.io/posts/switching-to-sampling-in-order-to-switch/)
- [Bayesian modelling cookbook](https://eigenfoo.xyz/bayesian-modelling-cookbook/)

## Dependencies

To use environment these materials were developed in:
`conda env create -f environment.yml`


### Acknowledgements

The King Markov story circulates on the internet in multiple formats. To the best of my knowledge [this book](https://xcelab.net/rm/statistical-rethinking/) is where it appeared first. The training series this talk is based on was first developed by Gijs Koot.